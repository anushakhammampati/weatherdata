#!/bin/sh

##################################################################################
#Title           :GenerateWeatherData.sh
###########################################
filePath=$1 #XML file path
tagName=$2  #Tag name to fetch values
echo "`awk '!/<.*>/' RS="<"$tagName">|</"$tagName">" $filePath`"
