###The code in this example demonstrates how to:

1. How to invoke python script from Unix Shell script.
2. Connect to a Python API's' to get weather data 
3. Store the data into output file Out_Weather_Report.txt .

## Getting Started

These instructions will get your project up and running on your device for development and testing purposes.

### Prerequisities

Python and its depend modules are given below
1. geopy
2. geocoder

###Note :This is tested on Mac OSx machine .However it will work on any favour of unix and Linux Operating systems.It is also tested on EC2 Linux AMI on AWS cloud .

##Grant Required permissions

chmod +x GenerateWeather.py
chmod +x GenerateWeatherData.sh

##Run Unix Shell script  
./GenerateWeatherData.sh



```
Output 

sh-3.2# ./GenerateWeatherData.sh
Adelaide
...
Melbourne
...
Sydney
...